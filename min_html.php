<?php
    class HTML {

        public function min($string = '') {
            if ( $this->min !== TRUE ) {
                return $string;
            }

            $reg_exp = array(
                "#\s//.*#", // удаляем комментарии "// comment"
                '#/\*(?:[^*]*(?:\*(?!/))*)*\*/#', // многострочные комментарии
                "/\>\s+\</", // пробелы между тегами
                "/\>\s+/",   // пробел после тега
                "/(\t|\n)/", // табуляция и перенос строки
                "/\s{2,}/"   // повторяющиеся пробелы
                );
            $replace = array('', '', '><', '>', '', ' ');

            return preg_replace($reg_exp, $replace, $string);
        }

    }
?>
